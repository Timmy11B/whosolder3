﻿using UnityEngine;
using System.Collections;

public class ShareButton : MonoBehaviour {
    public string ClickableURL = "https://twitter.com/whosolder";


    public void GoToUrl() {
        Application.OpenURL(ClickableURL);
    }

}
