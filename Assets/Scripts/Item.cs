﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public  class Item {

    public  string Name;
    public   int Searches;
    public Texture imag;
    public  Item(string name, int value, Texture img) {
        Name = name;
        Searches = value;
        imag = img;
    }
}
