﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class OneMoreChance : MonoBehaviour {

    public static OneMoreChance instance;
   public Button thisButton;
    public Text thisText;
    public Image thisImage;
    void Awake() {
        instance = this;


    }
    public void UseOneMoreChance() {
        thisButton.interactable = false;
        thisText.color = SetHiddenAlpha(thisText.color);
        thisImage.color = SetHiddenAlpha(thisText.color);

        PlayManager.instance.NewMatch();
        UnityRewardAds.instance.ShowRewardedAd(HandleShowResult);
        Debug.Log("DO THE UNITY VIDEO HERE");
        
    }


    public void EnableOneMoreChance() {
        thisButton.interactable = true;
        thisText.color = SetHiddenAlpha(thisText.color,false);
        thisImage.color = SetHiddenAlpha(thisText.color,false);
    }



    public Color SetHiddenAlpha(Color objColor, bool hidden = true) {
        if (hidden)
        {
            Color col = objColor;
            col.a = 0.3f;
            return col;
        }
        else {
            Color col = objColor;
            col.a = 1f;
            return col;
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:

              
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:

         
                break;
            case ShowResult.Failed:

           
                break;
        }
    }





}
